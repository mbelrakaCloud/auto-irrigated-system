import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SystemValues } from "../SystemValues";

@Component({
  selector: "app-monitor-dialog",
  templateUrl: "./monitor-dialog.component.html",
  styleUrls: ["./monitor-dialog.component.scss"],
})
export class MonitorDialogComponent implements OnInit {
  private temperature: number;
  private moisture: number;
  private fertilizer: number;
  private oxygen: number;
  private light: number;

  public get Temperature(): number {
    return (this.temperature = SystemValues.Instance.Temperature);
  }

  public set Temperature(temperature: number) {
    this.temperature = temperature;
  }

  public get Moisture(): number {
    return (this.moisture = SystemValues.Instance.Moisture);
  }

  public set Moisture(moisture: number) {
    this.moisture = moisture;
  }

  public get Fertilizer(): number {
    return (this.fertilizer = SystemValues.Instance.Fertilizer);
  }

  public set Fertilizer(fertilizer: number) {
    this.fertilizer = fertilizer;
  }

  public get Oxygen(): number {
    return (this.oxygen = SystemValues.Instance.Oxygen);
  }

  public set Oxygen(oxygen: number) {
    this.oxygen = oxygen;
  }

  public get Light(): number {
    return (this.light = SystemValues.Instance.Light);
  }

  public set Light(light: number) {
    this.light = light;
  }

  constructor(
    public dialogRef: MatDialogRef<MonitorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}
  ngOnInit() {}

  onCloseClick() {
    this.dialogRef.close({});
  }
}
