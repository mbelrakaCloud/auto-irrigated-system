import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-switch",
  templateUrl: "./switch.component.html",
  styleUrls: ["./switch.component.scss"],
})
export class SwitchComponent implements OnInit {
  private static instance: SwitchComponent;

  public static get Instance(): SwitchComponent {
    if (!this.instance) {
      this.instance = new SwitchComponent();
    }
    return this.instance;
  }

  private power: boolean;

  get Power(): boolean {
    return this.power;
  }

  set Power(power: boolean) {
    this.power = power;
  }

  constructor() {
    this.Power = true;
  }

  ngOnInit(): void {}

  togglePower() {
    this.Power = !this.Power;
  }
}
