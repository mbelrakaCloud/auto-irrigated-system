import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { MatDialogModule } from "@angular/material/dialog";
import { MatListModule } from "@angular/material/list";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule } from "@angular/forms";
import { AdminDialogueComponent } from "./admin-dialogue/admin-dialogue.component";
import { TemperatureSensorComponent } from "./Devices/Sensors/temperature-sensor/temperature-sensor.component";
import { MoistureSensorComponent } from "./Devices/Sensors/moisture-sensor/moisture-sensor.component";
import { FertilizerSensorComponent } from "./Devices/Sensors/fertilizer-sensor/fertilizer-sensor.component";
import { LightSensorComponent } from "./Devices/Sensors/light-sensor/light-sensor.component";
import { OxygenSensorComponent } from "./Devices/Sensors/oxygen-sensor/oxygen-sensor.component";
import { OxygenActuatorComponent } from "./Devices/Actuator/oxygen-actuator/oxygen-actuator.component";
import { TemperatureActuatorComponent } from "./Devices/Actuator/temperature-actuator/temperature-actuator.component";
import { LightActuatorComponent } from "./Devices/Actuator/light-actuator/light-actuator.component";
import { FertilizerActuatorComponent } from "./Devices/Actuator/fertilizer-actuator/fertilizer-actuator.component";
import { ServerComponent } from "./server/server.component";
import { AdminComponent } from "./admin/admin.component";
import { MonitorComponent } from "./monitor/monitor.component";
import { MoistureActuatorComponent } from "./Devices/Actuator/moisture-actuator/moisture-actuator.component";
import { SwitchComponent } from "./switch/switch.component";
import { MonitorDialogComponent } from "./monitor-dialog/monitor-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    AdminDialogueComponent,
    TemperatureSensorComponent,
    MoistureSensorComponent,
    FertilizerSensorComponent,
    LightSensorComponent,
    OxygenSensorComponent,
    OxygenActuatorComponent,
    TemperatureActuatorComponent,
    LightActuatorComponent,
    FertilizerActuatorComponent,
    ServerComponent,
    AdminComponent,
    MonitorComponent,
    MoistureActuatorComponent,
    SwitchComponent,
    MonitorDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    MatListModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AdminDialogueComponent],
})
export class AppModule {}
