import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { AdminDialogueComponent } from "./admin-dialogue/admin-dialogue.component";
import { SwitchComponent } from "./switch/switch.component";
import { TemperatureSensorComponent } from "./Devices/Sensors/temperature-sensor/temperature-sensor.component";
import { MoistureSensorComponent } from "./Devices/Sensors/moisture-sensor/moisture-sensor.component";
import { OxygenSensorComponent } from "./Devices/Sensors/oxygen-sensor/oxygen-sensor.component";
import { FertilizerSensorComponent } from "./Devices/Sensors/fertilizer-sensor/fertilizer-sensor.component";
import { LightSensorComponent } from "./Devices/Sensors/light-sensor/light-sensor.component";
import { ServerComponent } from "./server/server.component";
import { SystemValues } from "./SystemValues";
import { AdminComponent } from "./admin/admin.component";
import { MonitorDialogComponent } from "./monitor-dialog/monitor-dialog.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  constructor(public dialog: MatDialog) {
    this.feedInitialInputs();
    setInterval(this.feedInputs, 1000);
  }

  title = "auto-irrigation-system";

  switch = SwitchComponent.Instance;

  systemValues = SystemValues.Instance;

  // Initial Nutrients
  initialTemperature = 10;
  initialMoisture = 10;
  initialFertilizer = 10;
  initialOxygen = 10;
  initialLight = 10;

  // Sensors
  temperatureSensor = TemperatureSensorComponent.Instance;
  moistureSensor = MoistureSensorComponent.Instance;
  fertilizerSensor = FertilizerSensorComponent.Instance;
  oxygenSensor = OxygenSensorComponent.Instance;
  lightSensor = LightSensorComponent.Instance;

  // Server
  server = ServerComponent.Instance;

  ngOnInit(): void {}

  openAdminDialog(): void {
    const dialogRef = this.dialog.open(AdminDialogueComponent, {
      width: "500px",
      data: {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      AdminComponent.Instance.updatedNutrientLevels(result);
    });
  }

  openMonitorDialog(): void {
    this.dialog.open(MonitorDialogComponent, {
      width: "500px",
      data: {},
    });
  }

  feedInitialInputs() {
    if (SwitchComponent.Instance.Power) {
      SystemValues.Instance.Temperature = this.initialTemperature;

      SystemValues.Instance.Fertilizer = this.initialFertilizer;

      SystemValues.Instance.Moisture = this.initialMoisture;

      SystemValues.Instance.Oxygen = this.initialOxygen;

      SystemValues.Instance.Light = this.initialLight;
    }
  }

  feedInputs() {
    if (SwitchComponent.Instance.Power) {
      SystemValues.Instance.Temperature -= 1;

      SystemValues.Instance.Fertilizer -= 1;

      SystemValues.Instance.Moisture -= 1;

      SystemValues.Instance.Oxygen -= 1;

      SystemValues.Instance.Light -= 1;
    }
  }

  // Experiments

  experiment1Action() {
    SystemValues.Instance.Moisture = 2;
    SystemValues.Instance.Temperature = 4;
    SystemValues.Instance.Oxygen = 6;
    SystemValues.Instance.Fertilizer = 8;
    SystemValues.Instance.Light = 10;
  }

  experiment2Action() {
    SystemValues.Instance.Moisture = 2;
    SystemValues.Instance.Temperature = 2;
    SystemValues.Instance.Oxygen = 2;
    SystemValues.Instance.Fertilizer = 2;
    SystemValues.Instance.Light = 2;
  }

  experiment3Action() {
    SystemValues.Instance.Moisture = 2;
    SystemValues.Instance.Temperature = 2;
    SystemValues.Instance.Oxygen = 3;
    SystemValues.Instance.Fertilizer = 4;
    SystemValues.Instance.Light = 4;
  }
}
