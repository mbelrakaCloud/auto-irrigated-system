import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoistureSensorComponent } from './moisture-sensor.component';

describe('MoistureSensorComponent', () => {
  let component: MoistureSensorComponent;
  let fixture: ComponentFixture<MoistureSensorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoistureSensorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoistureSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
