import { Component, OnInit } from "@angular/core";
import { Sensor } from "src/app/Interfaces/Sensor";
import { Device } from "src/app/abstract-classes/Device";
import { SwitchComponent } from "src/app/switch/switch.component";
import { AppComponent } from "src/app/app.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-moisture-sensor",
  templateUrl: "./moisture-sensor.component.html",
  styleUrls: ["./moisture-sensor.component.scss"],
})
export class MoistureSensorComponent extends Device implements OnInit, Sensor {
  private static instance: MoistureSensorComponent;

  public static get Instance(): MoistureSensorComponent {
    if (!MoistureSensorComponent.instance) {
      MoistureSensorComponent.instance = new MoistureSensorComponent();
    }
    return MoistureSensorComponent.instance;
  }

  private moisture: number;

  switch = SwitchComponent.Instance;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  public get Moisture(): number {
    return this.moisture;
  }

  public set Moisture(moisture: number) {
    this.moisture = moisture;
  }

  measure() {
    this.Moisture = SystemValues.Instance.Moisture;
    return this.moisture;
  }
}
