import { Component, OnInit } from "@angular/core";
import { Sensor } from "../../../Interfaces/Sensor";
import { Device } from "src/app/abstract-classes/Device";
import { SwitchComponent } from "src/app/switch/switch.component";
import { AppComponent } from "src/app/app.component";
import { SystemValues } from "src/app/SystemValues";
@Component({
  selector: "app-temperature-sensor",
  templateUrl: "./temperature-sensor.component.html",
  styleUrls: ["./temperature-sensor.component.scss"],
})
export class TemperatureSensorComponent extends Device
  implements OnInit, Sensor {
  private static instance: TemperatureSensorComponent;

  public static get Instance(): TemperatureSensorComponent {
    if (!TemperatureSensorComponent.instance) {
      TemperatureSensorComponent.instance = new TemperatureSensorComponent();
    }
    return TemperatureSensorComponent.instance;
  }

  private temperature: number;

  switch = SwitchComponent.Instance;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  public get Temperature(): number {
    return this.temperature;
  }

  public set Temperature(temperature: number) {
    this.temperature = temperature;
  }

  measure() {
    this.Temperature = SystemValues.Instance.Temperature;
    return this.temperature;
  }
}
