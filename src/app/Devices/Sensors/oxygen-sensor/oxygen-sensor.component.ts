import { Component, OnInit } from "@angular/core";
import { Sensor } from "src/app/Interfaces/Sensor";
import { Device } from "src/app/abstract-classes/Device";
import { SwitchComponent } from "src/app/switch/switch.component";
import { AppComponent } from "src/app/app.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-oxygen-sensor",
  templateUrl: "./oxygen-sensor.component.html",
  styleUrls: ["./oxygen-sensor.component.scss"],
})
export class OxygenSensorComponent extends Device implements OnInit, Sensor {
  private static instance: OxygenSensorComponent;

  public static get Instance(): OxygenSensorComponent {
    if (!OxygenSensorComponent.instance) {
      OxygenSensorComponent.instance = new OxygenSensorComponent();
    }
    return OxygenSensorComponent.instance;
  }

  private oxygen: number;

  switch = SwitchComponent.Instance;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  public get Oxygen(): number {
    return this.oxygen;
  }

  public set Oxygen(oxygen: number) {
    this.oxygen = oxygen;
  }

  measure() {
    this.Oxygen = SystemValues.Instance.Oxygen;
    return this.oxygen;
  }
}
