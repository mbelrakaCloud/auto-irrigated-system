import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OxygenSensorComponent } from './oxygen-sensor.component';

describe('OxygenSensorComponent', () => {
  let component: OxygenSensorComponent;
  let fixture: ComponentFixture<OxygenSensorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OxygenSensorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OxygenSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
