import { Component, OnInit } from "@angular/core";
import { Sensor } from "src/app/Interfaces/Sensor";
import { Device } from "src/app/abstract-classes/Device";
import { SwitchComponent } from "src/app/switch/switch.component";
import { AppComponent } from "src/app/app.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-light-sensor",
  templateUrl: "./light-sensor.component.html",
  styleUrls: ["./light-sensor.component.scss"],
})
export class LightSensorComponent extends Device implements OnInit, Sensor {
  private light: number;

  private static instance: LightSensorComponent;

  public static get Instance(): LightSensorComponent {
    if (!LightSensorComponent.instance) {
      LightSensorComponent.instance = new LightSensorComponent();
    }
    return LightSensorComponent.instance;
  }

  switch = SwitchComponent.Instance;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  public get Light() {
    return this.light;
  }

  public set Light(light: number) {
    this.light = light;
  }

  measure() {
    this.Light = SystemValues.Instance.Light;
    return this.light;
  }
}
