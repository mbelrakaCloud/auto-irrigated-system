import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FertilizerSensorComponent } from './fertilizer-sensor.component';

describe('FertilizerSensorComponent', () => {
  let component: FertilizerSensorComponent;
  let fixture: ComponentFixture<FertilizerSensorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FertilizerSensorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FertilizerSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
