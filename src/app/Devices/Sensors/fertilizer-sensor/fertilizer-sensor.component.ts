import { Component, OnInit } from "@angular/core";
import { Sensor } from "src/app/Interfaces/Sensor";
import { Device } from "../../../abstract-classes/Device";
import { SwitchComponent } from "src/app/switch/switch.component";
import { AppComponent } from "src/app/app.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-fertilizer-sensor",
  templateUrl: "./fertilizer-sensor.component.html",
  styleUrls: ["./fertilizer-sensor.component.scss"],
})
export class FertilizerSensorComponent extends Device
  implements OnInit, Sensor {
  private fertilizer: number;

  private static instance: FertilizerSensorComponent;

  public static get Instance(): FertilizerSensorComponent {
    if (!FertilizerSensorComponent.instance) {
      FertilizerSensorComponent.instance = new FertilizerSensorComponent();
    }
    return FertilizerSensorComponent.instance;
  }

  switch = SwitchComponent.Instance;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  public get Fertilizer(): number {
    return this.fertilizer;
  }

  public set Fertilizer(fertilizer: number) {
    this.fertilizer = fertilizer;
  }

  measure() {
    this.Fertilizer = SystemValues.Instance.Fertilizer;
    return this.fertilizer;
  }
}
