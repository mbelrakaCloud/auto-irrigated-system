import { Component, OnInit } from "@angular/core";
import { Actuator } from "src/app/Interfaces/Actuator";
import { SwitchComponent } from "src/app/switch/switch.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-temperature-actuator",
  templateUrl: "./temperature-actuator.component.html",
  styleUrls: ["./temperature-actuator.component.scss"],
})
export class TemperatureActuatorComponent implements OnInit, Actuator {
  private desiredTemperature: number = 10;

  private static instance: TemperatureActuatorComponent;

  public static get Instance(): TemperatureActuatorComponent {
    if (!TemperatureActuatorComponent.instance) {
      TemperatureActuatorComponent.instance = new TemperatureActuatorComponent();
    }
    return TemperatureActuatorComponent.instance;
  }

  switch = SwitchComponent.Instance;

  public get DesiredTemperature() {
    return this.desiredTemperature;
  }

  public set DesiredTemperature(desiredTemperature: number) {
    this.desiredTemperature = desiredTemperature;
  }

  constructor() {}

  ngOnInit(): void {}

  Act() {
    SystemValues.Instance.Temperature += this.desiredTemperature;
    SystemValues.Instance.TemperatureFlag = false;
  }
}
