import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemperatureActuatorComponent } from './temperature-actuator.component';

describe('TemperatureActuatorComponent', () => {
  let component: TemperatureActuatorComponent;
  let fixture: ComponentFixture<TemperatureActuatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemperatureActuatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemperatureActuatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
