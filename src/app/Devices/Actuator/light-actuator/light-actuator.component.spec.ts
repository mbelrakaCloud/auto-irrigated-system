import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LightActuatorComponent } from './light-actuator.component';

describe('LightActuatorComponent', () => {
  let component: LightActuatorComponent;
  let fixture: ComponentFixture<LightActuatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LightActuatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightActuatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
