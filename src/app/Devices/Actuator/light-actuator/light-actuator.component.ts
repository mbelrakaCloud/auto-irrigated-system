import { Component, OnInit } from "@angular/core";
import { Actuator } from "src/app/Interfaces/Actuator";
import { Device } from "src/app/abstract-classes/Device";
import { SwitchComponent } from "src/app/switch/switch.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-light-actuator",
  templateUrl: "./light-actuator.component.html",
  styleUrls: ["./light-actuator.component.scss"],
})
export class LightActuatorComponent extends Device implements OnInit, Actuator {
  private desiredLightIntensity: number = 10;

  private static instance: LightActuatorComponent;

  public static get Instance(): LightActuatorComponent {
    if (!LightActuatorComponent.instance) {
      LightActuatorComponent.instance = new LightActuatorComponent();
    }
    return LightActuatorComponent.instance;
  }

  switch = SwitchComponent.Instance;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  public get DesiredLightIntensity(): number {
    return this.desiredLightIntensity;
  }

  public set DesiredLightIntensity(desiredLightIntensity: number) {
    this.desiredLightIntensity = desiredLightIntensity;
  }

  Act() {
    SystemValues.Instance.Light += this.desiredLightIntensity;
    SystemValues.Instance.LightFlag = false;
  }
}
