import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoistureActuatorComponent } from './moisture-actuator.component';

describe('MoistureActuatorComponent', () => {
  let component: MoistureActuatorComponent;
  let fixture: ComponentFixture<MoistureActuatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoistureActuatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoistureActuatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
