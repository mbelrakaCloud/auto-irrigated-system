import { Component, OnInit } from "@angular/core";
import { Actuator } from "src/app/Interfaces/Actuator";
import { SwitchComponent } from "src/app/switch/switch.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-moisture-actuator",
  templateUrl: "./moisture-actuator.component.html",
  styleUrls: ["./moisture-actuator.component.scss"],
})
export class MoistureActuatorComponent implements OnInit, Actuator {
  private static instance: MoistureActuatorComponent;

  public static get Instance(): MoistureActuatorComponent {
    if (!MoistureActuatorComponent.instance) {
      MoistureActuatorComponent.instance = new MoistureActuatorComponent();
    }
    return MoistureActuatorComponent.instance;
  }

  private desiredMoistureLevel: number = 10;

  switch = SwitchComponent.Instance;

  public get DesiredMoistureLevel(): number {
    return this.desiredMoistureLevel;
  }

  public set DesiredMoistureLevel(desiredMoistureLevel: number) {
    this.desiredMoistureLevel = desiredMoistureLevel;
  }

  constructor() {}

  ngOnInit(): void {}

  Act() {
    SystemValues.Instance.Moisture += this.desiredMoistureLevel;
    SystemValues.Instance.MoistureFlag = false;
  }
}
