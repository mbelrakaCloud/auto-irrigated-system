import { Component, OnInit } from "@angular/core";
import { Actuator } from "src/app/Interfaces/Actuator";
import { SwitchComponent } from "src/app/switch/switch.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-oxygen-actuator",
  templateUrl: "./oxygen-actuator.component.html",
  styleUrls: ["./oxygen-actuator.component.scss"],
})
export class OxygenActuatorComponent implements OnInit, Actuator {
  private desiredOxygenLevel: number = 10;

  private static instance: OxygenActuatorComponent;

  public static get Instance(): OxygenActuatorComponent {
    if (!OxygenActuatorComponent.instance) {
      OxygenActuatorComponent.instance = new OxygenActuatorComponent();
    }
    return OxygenActuatorComponent.instance;
  }

  switch = SwitchComponent.Instance;

  public get DesiredOxygenLevel(): number {
    return this.desiredOxygenLevel;
  }

  public set DesiredOxygenLevel(desiredOxygenLevel: number) {
    this.desiredOxygenLevel = desiredOxygenLevel;
  }

  constructor() {}

  ngOnInit(): void {}

  Act() {
    SystemValues.Instance.Oxygen += this.desiredOxygenLevel;
    SystemValues.Instance.OxygenFlag = false;
  }
}
