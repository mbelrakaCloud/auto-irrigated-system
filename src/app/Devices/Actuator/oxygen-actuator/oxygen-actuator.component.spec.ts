import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OxygenActuatorComponent } from './oxygen-actuator.component';

describe('OxygenActuatorComponent', () => {
  let component: OxygenActuatorComponent;
  let fixture: ComponentFixture<OxygenActuatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OxygenActuatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OxygenActuatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
