import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FertilizerActuatorComponent } from './fertilizer-actuator.component';

describe('FertilizerActuatorComponent', () => {
  let component: FertilizerActuatorComponent;
  let fixture: ComponentFixture<FertilizerActuatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FertilizerActuatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FertilizerActuatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
