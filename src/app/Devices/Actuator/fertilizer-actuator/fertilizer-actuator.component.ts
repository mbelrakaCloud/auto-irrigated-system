import { Component, OnInit } from "@angular/core";
import { Actuator } from "src/app/Interfaces/Actuator";
import { Device } from "src/app/abstract-classes/Device";
import { SwitchComponent } from "src/app/switch/switch.component";
import { AppComponent } from "src/app/app.component";
import { SystemValues } from "src/app/SystemValues";

@Component({
  selector: "app-fertilizer-actuator",
  templateUrl: "./fertilizer-actuator.component.html",
  styleUrls: ["./fertilizer-actuator.component.scss"],
})
export class FertilizerActuatorComponent extends Device
  implements OnInit, Actuator {
  private static instance: FertilizerActuatorComponent;

  private desiredFertilizerLevel: number = 10;

  switch = SwitchComponent.Instance;

  public static get Instance(): FertilizerActuatorComponent {
    if (!FertilizerActuatorComponent.instance) {
      FertilizerActuatorComponent.instance = new FertilizerActuatorComponent();
    }
    return FertilizerActuatorComponent.instance;
  }

  public get DesiredFertilizerLevel(): number {
    return this.desiredFertilizerLevel;
  }

  public set DesiredFertilizerLevel(desiredFertilizerLevel: number) {
    this.desiredFertilizerLevel = desiredFertilizerLevel;
  }

  constructor() {
    super();
  }

  ngOnInit(): void {}

  Act() {
    SystemValues.Instance.Fertilizer += this.DesiredFertilizerLevel;
    SystemValues.Instance.FertilizerFlag = false;
  }
}
