import { Component, OnInit } from "@angular/core";
import { SwitchComponent } from "../switch/switch.component";
import { TemperatureSensorComponent } from "../Devices/Sensors/temperature-sensor/temperature-sensor.component";
import { MoistureSensorComponent } from "../Devices/Sensors/moisture-sensor/moisture-sensor.component";
import { FertilizerSensorComponent } from "../Devices/Sensors/fertilizer-sensor/fertilizer-sensor.component";
import { OxygenSensorComponent } from "../Devices/Sensors/oxygen-sensor/oxygen-sensor.component";
import { LightSensorComponent } from "../Devices/Sensors/light-sensor/light-sensor.component";
import { TemperatureActuatorComponent } from "../Devices/Actuator/temperature-actuator/temperature-actuator.component";
import { MoistureActuatorComponent } from "../Devices/Actuator/moisture-actuator/moisture-actuator.component";
import { FertilizerActuatorComponent } from "../Devices/Actuator/fertilizer-actuator/fertilizer-actuator.component";
import { OxygenActuatorComponent } from "../Devices/Actuator/oxygen-actuator/oxygen-actuator.component";
import { LightActuatorComponent } from "../Devices/Actuator/light-actuator/light-actuator.component";
import { SystemValues } from "../SystemValues";

@Component({
  selector: "app-server",
  templateUrl: "./server.component.html",
  styleUrls: ["./server.component.scss"],
})
export class ServerComponent implements OnInit {
  private static instance: ServerComponent;

  public static get Instance(): ServerComponent {
    if (!ServerComponent.instance) {
      ServerComponent.instance = new ServerComponent();
    }
    return ServerComponent.instance;
  }

  // Desired Ntrient Levels

  private desiredTemperatureLevel: number = 0;
  private desiredMoistureLevel: number = 0;
  private desiredFertilizerLevel: number = 0;
  private desiredOxygenLevel: number = 0;
  private desiredLightIntensity: number = 0;

  // Nutrient Levels

  private temperatureLevel: number;
  private moistureLevel: number;
  private fertilizerLevel: number;
  private oxygenLevel: number;
  private lightIntensity: number;

  switch = SwitchComponent.Instance;

  // Sensors
  temperatureSensor = TemperatureSensorComponent.Instance;
  moistureSensor = MoistureSensorComponent.Instance;
  fertilizerSensor = FertilizerSensorComponent.Instance;
  oxygenSensor = OxygenSensorComponent.Instance;
  lightSensor = LightSensorComponent.Instance;

  public get DesiredTemperatureLevel(): number {
    return (this.desiredTemperatureLevel =
      SystemValues.Instance.DesiredTemperatureLevel);
  }

  public set DesiredTemperatureLevel(desiredTemperatureLevel: number) {
    this.desiredTemperatureLevel = desiredTemperatureLevel;
  }

  public get DesiredMoistureLevel(): number {
    return (this.desiredMoistureLevel =
      SystemValues.Instance.DesiredMoistureLevel);
  }

  public set DesiredMoistureLevel(desiredMoistureLevel: number) {
    this.desiredMoistureLevel = desiredMoistureLevel;
  }

  public get DesiredFertilizerLevel(): number {
    return (this.desiredFertilizerLevel =
      SystemValues.Instance.DesiredFertilizerLevel);
  }

  public set DesiredFertilizerLevel(desiredFertilizerLevel: number) {
    this.desiredFertilizerLevel = desiredFertilizerLevel;
  }

  public get DesiredOxygenLevel(): number {
    return (this.desiredOxygenLevel = SystemValues.Instance.DesiredOxygenLevel);
  }

  public set DesiredOxygenLevel(desiredOxygenLevel: number) {
    this.desiredOxygenLevel = desiredOxygenLevel;
  }

  public get DesiredLightIntensity(): number {
    return (this.desiredLightIntensity =
      SystemValues.Instance.DesiredLightLevel);
  }

  public set DesiredLightIntensity(desiredLightIntensity: number) {
    this.desiredLightIntensity = desiredLightIntensity;
  }

  public get TemperatureLevel(): number {
    return this.temperatureLevel;
  }

  public set TemperatureLevel(temperatureLevel: number) {
    this.temperatureLevel = temperatureLevel;
  }

  public get MoistureLevel(): number {
    return this.moistureLevel;
  }

  public set MoistureLevel(moistureLevel: number) {
    this.moistureLevel = moistureLevel;
  }

  public get FertilizerLevel(): number {
    return this.fertilizerLevel;
  }

  public set FertilizerLevel(fertilizerLevel: number) {
    this.fertilizerLevel = fertilizerLevel;
  }

  public get OxygenLevel(): number {
    return this.oxygenLevel;
  }

  public set OxygenLevel(oxygenLevel: number) {
    this.oxygenLevel = oxygenLevel;
  }

  public get LightIntensity(): number {
    return this.lightIntensity;
  }

  public set LightIntensity(lightIntensity: number) {
    this.lightIntensity = lightIntensity;
  }

  constructor() {
    setInterval(() => {
      this.compareData();
    }, 1000);
  }

  ngOnInit() {}

  compareData() {
    if (!SwitchComponent.Instance.Power) {
      return;
    }
    this.compareTemperatureLevel();
    this.compareMoistureLevel();
    this.compareFertilizerLevel();
    this.compareOxygenLevel();
    this.compareLightIntensity();

    this.takeActions();
  }
  compareTemperatureLevel() {
    this.TemperatureLevel = TemperatureSensorComponent.Instance.measure();

    if (this.TemperatureLevel <= this.DesiredTemperatureLevel) {
      SystemValues.Instance.TemperatureFlag = true;
    }
  }
  compareMoistureLevel() {
    this.MoistureLevel = MoistureSensorComponent.Instance.measure();

    if (this.MoistureLevel <= this.DesiredMoistureLevel) {
      SystemValues.Instance.MoistureFlag = true;
    }
  }
  compareFertilizerLevel() {
    this.FertilizerLevel = FertilizerSensorComponent.Instance.measure();

    if (this.FertilizerLevel <= this.DesiredFertilizerLevel) {
      SystemValues.Instance.FertilizerFlag = true;
    }
  }
  compareOxygenLevel() {
    this.oxygenLevel = OxygenSensorComponent.Instance.measure();

    if (this.OxygenLevel <= this.DesiredOxygenLevel) {
      SystemValues.Instance.OxygenFlag = true;
    }
  }
  compareLightIntensity() {
    this.LightIntensity = LightSensorComponent.Instance.measure();

    if (this.LightIntensity <= this.DesiredLightIntensity) {
      SystemValues.Instance.LightFlag = true;
    }
  }

  public takeActions() {
    if (!SwitchComponent.Instance.Power) {
      return;
    }
    this.actOnTemperature();
    this.actOnMoisture();
    this.actOnFertilizer();
    this.actOnOxygen();
    this.actOnLight();
  }

  actOnTemperature() {
    if (SystemValues.Instance.TemperatureFlag) {
      TemperatureActuatorComponent.Instance.Act();
      SystemValues.Instance.TemperatureFlag = false;
    }
  }
  actOnMoisture() {
    if (SystemValues.Instance.MoistureFlag) {
      MoistureActuatorComponent.Instance.Act();
      SystemValues.Instance.MoistureFlag = false;
    }
  }
  actOnFertilizer() {
    if (SystemValues.Instance.FertilizerFlag) {
      FertilizerActuatorComponent.Instance.Act();
      SystemValues.Instance.FertilizerFlag = false;
    }
  }
  actOnOxygen() {
    if (SystemValues.Instance.OxygenFlag) {
      OxygenActuatorComponent.Instance.Act();
      SystemValues.Instance.OxygenFlag = false;
    }
  }
  actOnLight() {
    if (SystemValues.Instance.LightFlag) {
      LightActuatorComponent.Instance.Act();
      SystemValues.Instance.LightFlag = false;
    }
  }
}
