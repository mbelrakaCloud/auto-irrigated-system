import { Component, OnInit } from "@angular/core";
import { SystemValues } from "../SystemValues";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"],
})
export class AdminComponent implements OnInit {
  private static instance: AdminComponent;

  public static get Instance(): AdminComponent {
    if (!AdminComponent.instance) {
      AdminComponent.instance = new AdminComponent();
    }
    return AdminComponent.instance;
  }

  private desiredTemperatureLevel: number;
  private desiredMoistureLevel: number;
  private desiredFertilizerLevel: number;
  private desiredOxygenLevel: number;
  private desiredLightLevel: number;

  public get DesiredTemperatureLevel(): number {
    return this.desiredTemperatureLevel;
  }

  public set DesiredTemperatureLevel(desiredTemperatureLevel: number) {
    this.desiredTemperatureLevel = desiredTemperatureLevel;
  }

  public get DesiredMoistureLevel(): number {
    return this.desiredMoistureLevel;
  }

  public set DesiredMoistureLevel(desiredMoistureLevel: number) {
    this.desiredMoistureLevel = desiredMoistureLevel;
  }

  public get DesiredFertilizerLevel(): number {
    return this.desiredFertilizerLevel;
  }

  public set DesiredFertilizerLevel(desiredFertilizerLevel: number) {
    this.desiredFertilizerLevel = desiredFertilizerLevel;
  }

  public get DesiredOxygenLevel(): number {
    return this.desiredOxygenLevel;
  }

  public set DesiredOxygenLevel(desiredOxygenLevel: number) {
    this.desiredOxygenLevel = desiredOxygenLevel;
  }

  public get DesiredLightLevel(): number {
    return this.desiredLightLevel;
  }

  public set DesiredLightLevel(desiredLightLevel: number) {
    this.desiredLightLevel = desiredLightLevel;
  }

  constructor() {}

  ngOnInit() {}

  updatedNutrientLevels(result) {
    SystemValues.Instance.DesiredMoistureLevel = this.DesiredMoistureLevel = parseInt(
      result.moisture
    );
    SystemValues.Instance.DesiredTemperatureLevel = this.DesiredTemperatureLevel = parseInt(
      result.temperature
    );
    SystemValues.Instance.DesiredOxygenLevel = this.DesiredOxygenLevel = parseInt(
      result.oxygen
    );
    SystemValues.Instance.DesiredFertilizerLevel = this.DesiredFertilizerLevel = parseInt(
      result.fertilizer
    );
    SystemValues.Instance.DesiredLightLevel = this.DesiredLightLevel = parseInt(
      result.light
    );
  }
}
