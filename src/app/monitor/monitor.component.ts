import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-monitor",
  templateUrl: "./monitor.component.html",
  styleUrls: ["./monitor.component.scss"],
})
export class MonitorComponent implements OnInit {
  // Nutrients
  private temperatureLevel: number;
  private moistureLevel: number;
  private fertilizerLevel: number;
  private oxygenLevel: number;
  private lightIntensity: number;

  public get TemperatureLevel(): number {
    return this.temperatureLevel;
  }

  public set TemperatureLevel(temperatureLevel: number) {
    this.temperatureLevel = temperatureLevel;
  }

  public get MoistureLevel(): number {
    return this.moistureLevel;
  }

  public set MoistureLevel(moistureLevel: number) {
    this.moistureLevel = moistureLevel;
  }

  public get FertilizerLevel(): number {
    return this.fertilizerLevel;
  }

  public set FertilizerLevel(fertilizerLevel: number) {
    this.fertilizerLevel = fertilizerLevel;
  }

  public get OxygenLevel(): number {
    return this.oxygenLevel;
  }

  public set OxygenLevel(oxygenLevel: number) {
    this.oxygenLevel = oxygenLevel;
  }

  public get LightIntensity(): number {
    return this.lightIntensity;
  }

  public set LightIntensity(lightIntensity: number) {
    this.lightIntensity = lightIntensity;
  }

  constructor() {}

  ngOnInit() {}

  displayData() {}
}
