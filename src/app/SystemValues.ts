export class SystemValues {
  private static instance: SystemValues;

  public static get Instance(): SystemValues {
    if (!SystemValues.instance) {
      SystemValues.instance = new SystemValues();
    }
    return SystemValues.instance;
  }

  private temperature: number;
  private moisture: number;
  private fertilizer: number;
  private oxygen: number;
  private light: number;

  private desiredTemperatureLevel: number = 0;
  private desiredMoistureLevel: number = 0;
  private desiredFertilizerLevel: number = 0;
  private desiredOxygenLevel: number = 0;
  private desiredLightLevel: number = 0;

  private temperatureFlag: boolean = false;
  private moistureFlag: boolean = false;
  private fertilizerFlag: boolean = false;
  private oxygenFlag: boolean = false;
  private lightFlag: boolean = false;

  public get Temperature(): number {
    return this.temperature;
  }

  public set Temperature(temperature: number) {
    this.temperature = temperature;
  }

  public get Moisture(): number {
    return this.moisture;
  }

  public set Moisture(moisture: number) {
    this.moisture = moisture;
  }

  public get Fertilizer(): number {
    return this.fertilizer;
  }

  public set Fertilizer(fertilizer: number) {
    this.fertilizer = fertilizer;
  }

  public get Oxygen(): number {
    return this.oxygen;
  }

  public set Oxygen(oxygen: number) {
    this.oxygen = oxygen;
  }

  public get Light(): number {
    return this.light;
  }

  public set Light(light: number) {
    this.light = light;
  }

  public get TemperatureFlag(): boolean {
    return this.temperatureFlag;
  }

  public set TemperatureFlag(temperatureFlag: boolean) {
    this.temperatureFlag = temperatureFlag;
  }

  public get MoistureFlag(): boolean {
    return this.moistureFlag;
  }

  public set MoistureFlag(moistureFlag: boolean) {
    this.moistureFlag = moistureFlag;
  }

  public get FertilizerFlag(): boolean {
    return this.fertilizerFlag;
  }

  public set FertilizerFlag(fertilizerFlag: boolean) {
    this.fertilizerFlag = fertilizerFlag;
  }

  public get OxygenFlag(): boolean {
    return this.oxygenFlag;
  }

  public set OxygenFlag(oxygenFlag: boolean) {
    this.oxygenFlag = oxygenFlag;
  }

  public get LightFlag(): boolean {
    return this.lightFlag;
  }

  public set LightFlag(lightFlag: boolean) {
    this.lightFlag = lightFlag;
  }

  public get DesiredTemperatureLevel(): number {
    return this.desiredTemperatureLevel;
  }

  public set DesiredTemperatureLevel(desiredTemperatureLevel: number) {
    this.desiredTemperatureLevel = desiredTemperatureLevel;
  }

  public get DesiredMoistureLevel(): number {
    return this.desiredMoistureLevel;
  }

  public set DesiredMoistureLevel(desiredMoistureLevel: number) {
    this.desiredMoistureLevel = desiredMoistureLevel;
  }

  public get DesiredFertilizerLevel(): number {
    return this.desiredFertilizerLevel;
  }

  public set DesiredFertilizerLevel(desiredFertilizerLevel: number) {
    this.desiredFertilizerLevel = desiredFertilizerLevel;
  }

  public get DesiredOxygenLevel(): number {
    return this.desiredOxygenLevel;
  }

  public set DesiredOxygenLevel(desiredOxygenLevel: number) {
    this.desiredOxygenLevel = desiredOxygenLevel;
  }

  public get DesiredLightLevel(): number {
    return this.desiredLightLevel;
  }

  public set DesiredLightLevel(desiredLightLevel: number) {
    this.desiredLightLevel = desiredLightLevel;
  }
}
