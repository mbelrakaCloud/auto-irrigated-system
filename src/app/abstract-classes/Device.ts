import { SwitchComponent } from "../switch/switch.component";

export abstract class Device {
  private name: string;

  switch = SwitchComponent.Instance;

  constructor() {}

  public getName(): string {
    return this.name;
  }

  public setName(name): void {
    this.name = name;
  }

  public measure(): any {}
}
