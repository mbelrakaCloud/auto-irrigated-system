import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDialogueComponent } from './admin-dialogue.component';

describe('AdminDialogueComponent', () => {
  let component: AdminDialogueComponent;
  let fixture: ComponentFixture<AdminDialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
