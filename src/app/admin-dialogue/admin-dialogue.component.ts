import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: "app-admin-dialogue",
  templateUrl: "./admin-dialogue.component.html",
  styleUrls: ["./admin-dialogue.component.scss"],
})
export class AdminDialogueComponent implements OnInit {
  moisture;
  temperature;
  oxygen;
  fertilizer;
  light;

  constructor(
    public dialogRef: MatDialogRef<AdminDialogueComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}
  ngOnInit(): void {}

  onSaveClick(): void {
    this.dialogRef.close({
      moisture: this.moisture,
      temperature: this.temperature,
      oxygen: this.oxygen,
      fertilizer: this.fertilizer,
      light: this.light,
    });
  }
}
