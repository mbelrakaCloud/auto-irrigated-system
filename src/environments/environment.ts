// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: "AIzaSyB6ku24BrFbf-Hd3DaDizG7M2WDwHLnJDk",
	authDomain: "auto-irrigation-syystem.firebaseapp.com",
	databaseURL: "https://auto-irrigation-syystem.firebaseio.com",
	projectId: "auto-irrigation-syystem",
	storageBucket: "auto-irrigation-syystem.appspot.com",
	messagingSenderId: "588899416364",
	appId: "1:588899416364:web:7085a6f3d6a55d7bca5489"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
